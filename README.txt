
### ABOUT

  This module adds the necessary script to the footer of
  ones site for prompting users to chat with virtual assistant ASKOM.

  The Agent virtual by ASKOM module is a relationship clients in 
  automatic chat available 24 hours a day and 7 days a week service. 
  No Advisor does need to be available, a virtual agent is responsible
  to interact with your visitors. It is capable of processing recurring
  questions and saves time for humans to focus on issues has high added value

	Benefits
	o Reduce your costs of contact
	o Decrease your bounce rate
	o Humanize your site test for free the ASKOM solution 
	through a 100% Free version


### INSTALLING

  1. Extract askom.zip into your sites/all/modules directory so it looks like 
  sites/all/modules/askom
  2. Navigate to Administration -> Modules and enable the module.
  3. Navigate to Administration -> Configuration -> System -> Askom and add 
  your account ID as well as any other configuration options you want.

### CREDITS

  Originally developed by ASKOM - http://www.askom-solutions.com/
