<?php

/**
 * @file
 * Administrative page callbacks for the askom module.
 */

/**
 * Implements askom_admin_settings_form().
 * @return array
 *   form
 */
function askom_admin_settings_form() {
  $form['askom'] = array(
    '#type' => 'vertical_tabs',
  );

  // General Settings.
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
    '#group' => 'askom',
  );
  $form['account']['askom_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Askom ID'),
    '#default_value' => variable_get('askom_account', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => '<p>' . t('The account number is unique to the websites domain and can be found in the script given to you by the askom dashboard settings.') . '</p>' .
    '<p>' . t('Go to <a href="http://www.askom.fr/solution/subscribe-free?utm_source=drupal&utm_medium=catalogueaddon&utm_campaign=cms" target="_blank">Askom website</a>, create your account and copy your implementing code (ASKOM id)') . '</p>',
  );
  // END General Settings.
  return system_settings_form($form);
}

/**
 * Implements askom_admin_settings_form_validate().
 *
 * @param array $form
 *   form
 * @param array $form_state
 *   state
 */
function askom_admin_settings_form_validate($form, &$form_state) {
  if (empty($form_state['values']['askom_account'])) {
    form_set_error('askom_account', t('A valid askom account number is needed.'));
  }
}
